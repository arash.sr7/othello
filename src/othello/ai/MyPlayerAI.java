package othello.ai;

import java.awt.Point;

import othello.model.Board;

// your AI here. currently will choose first possible move
public class MyPlayerAI extends ReversiAI {

	@Override
	public Point nextMove(Board b) {
	//	for (int j = 0; j < size; j++)
	//		for (int i = 0; i < size; i++)
	//			if (b.move(i, j))
	//				return new Point(i, j);
	//	return null;
        double alpha=-100000;
        double beta=100000;
        double tmp;
        int depth=0;
        Point cur_mv=null;
        double max_v=-100000;
        for (int j = 0; j < size; j++) {
            for (int i = 0; i < size; i++) {
                if (b.move(i, j))
                {
                    tmp=min(b,alpha,beta,depth);
                    if(tmp>max_v){
                        max_v=tmp;
                        cur_mv=new Point(i,j);
                    }
                    if (max_v>beta)
                        return cur_mv;
                    alpha = Math.max(max_v,alpha);
                }
            }
        }
        return cur_mv;



		/*{
			b.isCapturedByMe(x, y);					// square (x, y) is mine
			b.isCapturedByMyOppoenet(x, y);			// square (x, y) is for my opponent
			b.isEmptySquare(x, y);					// square (x, y) is empty
			b.move(x, y);							// attempt to place a piece at specified coordinates, and update
													// the board appropriately, or return false if not possible
			b.turn();								// end current player's turn
			b.print();								// ASCII printout of the current board
			if(b.getActive() == Board.WHITE)		//I am White
			if(b.getActive() == Board.BLACK)		//I am Black
			
			b.getMoveCount(true);					//number of possible moves for me
			b.getMoveCount(false);					//number of possible moves for my opponent
			b.getTotal(true);						//number of cells captured by me
			b.getTotal(false);						//number of cells captured by my opponent
			this.size;								//board size (always 8)
		}*/
	}
	private double max(Board b, double alpha, double beta,int depth){

		double max_v=-100000;

		if (depth==9){
			return b.getScore();
		}
		depth++;
		for (int j = 0; j < size; j++) {
			for (int i = 0; i < size; i++) {
				if (b.move(i, j))
				{
					max_v = Math.max(min(b,alpha,beta,depth),max_v);
					if (max_v>beta)
					    return max_v;
					alpha = Math.max(max_v,alpha);
				}
			}
		}
		if (max_v == -100000)
		    return 100000;
		else
		    return max_v;
	}


	private double min(Board b, double alpha, double beta,int depth){

		double min_v=100000;

		if (depth==9){
			return b.getScore();
		}
		depth++;
		for (int j = 0; j < size; j++) {
			for (int i = 0; i < size; i++) {
				if (b.move(i, j))
				{
					min_v=Math.min(max(b,alpha,beta,depth),min_v);
					if (min_v<alpha)
					    return min_v;
					beta = Math.min(min_v,beta);
				}
			}
		}
		if (min_v>100000)
		    return -100000;
		else
		    return min_v;
	}

	@Override
	public String getName() {
		//IMPORTANT: your student number here
		return new String("9300000");
	}
}
